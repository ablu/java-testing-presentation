#!/bin/bash

cat "$1" | sed 's/display:none/display:inline/g' > "$1".tmp
BASENAME=`echo $1 | sed 's/\..*$//'`
LAYERS=$(inkscape --query-all $1.tmp |grep ^layer | awk -F, '{print $1}')
C=1
if [ $(echo $LAYERS | wc -w) -gt 1 ]
then
    for i in $LAYERS
    do
            inkscape $1.tmp -i $i -j -C --export-png=${BASENAME}-layer$C.png || exit 1
    	C=$((C+1))
    done
fi

# workaround for https://bugs.launchpad.net/ubuntu/+source/inkscape/+bug/1417470
inkscape $1.tmp -j -C --export-latex --export-ps=$BASENAME.ps
ps2pdf $BASENAME.ps $BASENAME.pdf

sed "s/[.]ps/.pdf/g" $BASENAME.ps_tex > $BASENAME.pdf_tex

rm $BASENAME.ps_tex $BASENAME.ps

rm $1.tmp
