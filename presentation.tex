\documentclass[14pt,aspectratio=169]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{smartdiagram}
\usesmartdiagramlibrary{additions}
\usepackage{listings}
\lstset{%
language={},%
basicstyle=\small\ttfamily,%
keywordstyle=\ttfamily,%
identifierstyle=\ttfamily,%
commentstyle=\itshape,%
showstringspaces=false,%
numbers=left,%
numberstyle=\ttfamily\footnotesize,%
stringstyle=\ttfamily,%
numbersep=1em,%
xleftmargin=12mm,%
breaklines=true,%
breakatwhitespace=true,%
captionpos=b,
escapeinside={<@}{@>}
}

\graphicspath{{figures/}}

\usepackage{pdfpcnotes}
\usepackage{tabularx}
\usepackage{caption}

\makeatletter
\renewcommand\footnotesize{%
   \@setfontsize\footnotesize\@ixpt{11}%
   \abovedisplayskip 8\p@ \@plus2\p@ \@minus4\p@
   \abovedisplayshortskip \z@ \@plus\p@
   \belowdisplayshortskip 4\p@ \@plus2\p@ \@minus2\p@
   \def\@listi{\leftmargin\leftmargini
               \topsep 4\p@ \@plus2\p@ \@minus2\p@
               \parsep 2\p@ \@plus\p@ \@minus\p@
               \itemsep \parsep}%
   \belowdisplayskip \abovedisplayskip
}
\makeatother

\mode<presentation> {
	\setbeamertemplate{footline}{
	  \begin{beamercolorbox}[sep=-2ex]{author in head/foot}
	    \hfill
	    %\normalsize\textit{\insertsection}
	    \hfill
	    \llap{\large\insertpagenumber}
	    \hspace{5mm}
	    \vspace{5mm}
	  \end{beamercolorbox}
	}
	\setbeamertemplate{navigation symbols}{}
}

\title{(Automated) Testing in Java}

\author{Erik Schilling
\\
\href{mailto:ablu@mail.upb.de}{ablu@mail.upb.de}}
\date{\today}

\begin{document}
\smartdiagramset{
back arrow disabled=true,
module minimum width=2cm,
text width=4cm,
}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
    \frametitle{Why write tests?}

    \begin{center}
        \smartdiagram[circular diagram]{Develop,Start Application,Manual Test}
    \end{center}

    \begin{itemize}
        \item how to test a library (no application existing yet)?
        \item tedious to repeat
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Why write tests?}

    \begin{center}
        \smartdiagram[circular diagram]{Develop,Automatic Test}
    \end{center}

    \begin{itemize}
        \item write test once
        \item repeated execution is cheap
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Why automate tests?}


    \begin{center}
        \def\svgwidth{1\columnwidth}
        \input{figures/effort-graph.pdf_tex}
    \end{center}

    \begin{itemize}
        \item not considered here: ``too boring to do manual test''
    \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Overview}
\tableofcontents[
sectionstyle=show/show,
subsectionstyle=hide/hide,
]
\end{frame}

\section{Theory}
\begin{frame}[fragile]
    \frametitle{Test theory}

    \begin{columns}
        \begin{column}{0.5\textwidth}
        \begin{lstlisting}
int increase(int a) {
    if (a > 3) {
        return 2 * a;
    }
    return 3 * a;
}
        \end{lstlisting}
        \end{column}
        \begin{column}{0.5\textwidth}
            \only<3->{
            equivalence classes:
            \begin{itemize}
                \item $> 3$: representative: $5$
                \item $\leq 3$: representative: $1$
            \end{itemize}
            Now only 2 values to test!
            }
        \end{column}
    \end{columns}

    \begin{itemize}
        \item which values to test?
        \item $\infty$ possibilities $\rightarrow$ infeasible
    \end{itemize}

    \vspace{1cm}

    \only<2->{\textbf{Build equivalence classes!}}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Boundary-value analysis}
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
        \begin{lstlisting}
int increase(int a) {
    if (a > 3) {
        return 2 * a;
    }
    return 3 * a;
}
        \end{lstlisting}
        \end{column}
        \begin{column}{0.5\textwidth}
            \pause
            boundary values:
            \begin{itemize}
                \item 4: first larger value
                \item 3: directly on the border
                \item 2: first smaller value
                \pause
                \item 0, -1: \textit{special} numbers
            \end{itemize}
        \end{column}
    \end{columns}

    \pause
    \begin{center}
        \def\svgwidth{1\columnwidth}
        \input{figures/boundary-value-analysis.pdf_tex}
        \vspace{-1.5cm}
    \end{center}

    \pause
    \small
    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item often not applicable (usually code is not that simple)
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item in our case especially neutral group elements might be interesting candidates
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Example}

    \begin{lstlisting}[basicstyle=\tiny,numberstyle=\ttfamily\tiny]
/**
 * Queries an array and returns the larger part of it
 * @param pivot the element which is used for the comparision
 * @param array the array to query
 * @return a list of elements which are larger <@\only<2->{\textcolor{red}{or equal}}@><@\only<1>{or equal}@> to the pivot element
 */
private static List<Integer> getLargerPart(int pivot, int[] array) {
    List<Integer> list = new ArrayList<>();
    for (int element : array) {
        if (element <@\only<2->{\textcolor{red}{$<$}}@><@\only<1>{$<$}@> pivot) {
            list.add(element);
        }
    }
    return list;
}
    \end{lstlisting}

    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item equivalence classes?
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item boundary values?
            \end{itemize}
        \end{column}
    \end{columns}

    \pause

    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item identified test cases from code
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{itemize}
                \item \textcolor{red}{but: code is wrong!}
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Blackbox vs Whitebox tests}

    \begin{columns}[T]
        \begin{column}{0.5\textwidth}
            Whitebox:
            \begin{itemize}
                \item derive test cases from code
                \item ensures that code logic is tested
                \item code might not follow specification
            \end{itemize}
        \end{column}
        \begin{column}{0.5\textwidth}
            Blackbox:
            \begin{itemize}
                \item derive test cases from specification
                \item ensure that code does what it should
                \item might not test all code branches
            \end{itemize}
        \end{column}
    \end{columns}
\end{frame}

\section{Writing Tests in Java}

\begin{frame}
    \frametitle{JUnit}

    General introduction skipped here:

    \large Look at the official User Guide\footnote{\url{http://junit.org/junit5/docs/current/user-guide/}}

\end{frame}

\begin{frame}[fragile]
    \frametitle{Mocking}

    \begin{lstlisting}
class Combiner {
    int combine(int a, int b) {
        return a + b;
    }
}
    \end{lstlisting}

    Situation:
    \begin{itemize}
        \item \texttt{Combiner} is well tested already
        \item \texttt{Combiner} is really slow or depends on random
    \end{itemize}

    \begin{center}
        \large
        Wouldn't it be nice to test code which uses \texttt{combine} without retesting that method?
    \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Mocking -- Example}
    \begin{lstlisting}
class CombinerUser {
    private final Combiner combiner;
    public CombinerUser(Combiner combiner) {
        this.combiner = combiner;
    }

    public int combine(int a, int b) {
        return combiner.combine(a, b);
    }
}
    \end{lstlisting}
    \begin{lstlisting}
class Combiner {
    int combine(int a, int b) {
        return a + b;
    }
}
    \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Mocking -- Example}

    We want to prevent this:
    \begin{lstlisting}
@Test
void testNaive() {
    final Combiner combiner = new Combiner();
    final CombinerUser user = new CombinerUser(combiner);

    final int result = user.combine(1, 2);
    assertEquals(3, result);
}
    \end{lstlisting}

\begin{columns}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item the internal \texttt{combine} is already tested!
        \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
        \begin{itemize}
            \item test will be slow is the internal \texttt{combine} is slow
        \end{itemize}
    \end{column}
\end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Solution 1: Inversion of Control (IoC)}
    \begin{lstlisting}
class AddCombiner implements Combiner {
    @Override
    public int combine(int a, int b) {
        return a + b;
    }
}

interface Combiner {
    int combine(int a, int b);
}
    \end{lstlisting}

    The test can now define the result of the \texttt{combine} method:
    \begin{lstlisting}
final CombinerUser user =
        new CombinerUser((a, b) -> <@\textcolor{red}{5}@>);
assertEquals(<@\textcolor{red}{5}@>, user.combine(1, 2));
    \end{lstlisting}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Solution 2: Mocking}

    Use EasyMock\footnote{\url{http://easymock.org/user-guide.html\#mocking}} to mock the \texttt{Combiner} class.

    \begin{lstlisting}
final Combiner mock = mock(Combiner.class);
<@\textcolor{red}{expect(mock.combine(1, 2))}@> // <@\textcolor{blue}{expect this call}@>
        <@\textcolor{red}{.andReturn(5);}@> // <@\textcolor{blue}{and let it return 5}@>
replay(mockedCombiner); // start replay mode

final CombinerUser user =
        new CombinerUser(mock);
final int result = user.combine(1, 2);
verify(mockedCombiner);
assertEquals(<@\textcolor{red}{5}@>, result);
    \end{lstlisting}
\end{frame}

\begin{frame}
    \frametitle{Mocking -- Conclusion}
    Benefits:
    \begin{itemize}
        \item prevents double testing
        \item \textit{can} lower maintenance effort
        \item speeds up tests
        \item can test that a certain code path was invoked
    \end{itemize}

    \large
    Many nice features, but overuse can result in higher maintenance effort!
\end{frame}

\begin{frame}
    \frametitle{Mocking -- Conclusion}
    \begin{itemize}
        \item prevents double testing
        \item \textit{can} lower maintenance effort
        \item speeds up tests
        \item can test that a certain code path was invoked
    \end{itemize}

    \large
    Many nice features, but overuse can result in higher maintenance effort!
\end{frame}

\begin{frame}[fragile]
    \frametitle{Better error messages}
    \begin{columns}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{assertNotNull(foo);}
            \vspace{0.2cm}

            \normalsize
            Message:

            \small
            \texttt{expected: not <null>}
        \end{column}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{assertNotNull(foo, "foo");}
            \vspace{0.2cm}

            \normalsize
            Message:

            \small
            \texttt{foo ==> expected: not <null>}
        \end{column}
    \end{columns}

    \vspace{1cm}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{assertFalse(foo);}
            \vspace{0.2cm}

            \normalsize
            Message:

            \small
            \texttt{AssertionFailedError}
        \end{column}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{assertTrue(bar,}

            \texttt{    "bar should be true " +}

            \texttt{    "because ...");}
            \vspace{0.2cm}

            \normalsize
            Message:

            \small
            \texttt{bar should be true because ...}
        \end{column}
    \end{columns}

    \vspace{1cm}

    \begin{columns}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{fail();}\footnote[frame]{No longer possible in JUnit5 anyway}
        \end{column}
        \begin{column}{0.5\textwidth}
            \small
            \texttt{fail("Failed to test ...");}
        \end{column}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Better error messages}


    \begin{columns}
        \begin{column}{0.4\textwidth}
            \small
            \texttt{assertTrue(a \&\& b);}
        \end{column}
        \begin{column}{0.6\textwidth}
            \small
            \texttt{assertTrue(a, "a should be true");}

            \texttt{assertTrue(b, "b should be true");}
        \end{column}
    \end{columns}

    \vspace{1cm}

    \begin{itemize}
        \item do not catch exceptions only to log them
        \begin{itemize}
            \item add \texttt{throws} clause
            \item JUnit logs them for you
        \end{itemize}
    \end{itemize}
\end{frame}

\end{document}
