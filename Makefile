SVG_FIGURES := $(wildcard figures/*.svg)
FIGURES := $(SVG_FIGURES:%.svg=%.pdf_tex)

all: build/presentation.pdf $(FIGURES)

dirs:
	mkdir -p aux/ build/

build/%.pdf: %.tex $(FIGURES) dirs
	latexmk -output-directory=aux/ -interaction=nonstopmode -pdf $< && \
	cp aux/$(notdir $@)* build/

%.pdf_tex: %.svg build-svg.sh
	./build-svg.sh $<

clean:
	git clean -dxf
